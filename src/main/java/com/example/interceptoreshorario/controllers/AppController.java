package com.example.interceptoreshorario.controllers;

import jakarta.servlet.http.HttpServletRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/api")
public class AppController {
    @GetMapping("/foo")
    public ResponseEntity<?> foo(HttpServletRequest request){
        Map<String, Object> objectMap = new HashMap<>();
        objectMap.put("title", "Bienvenido al sistema");
        objectMap.put("time", new Date());
        objectMap.put("message", request.getAttribute("message"));
//       Map<String, Object> objectMap = Collections.singletonMap("title", "Sistema de atención");
       return  ResponseEntity.ok(objectMap);
    }
}
